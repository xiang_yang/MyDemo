package com.xy.demo.config;

import com.xy.demo.util.LogUtils;

/**
 * Created by xy on 2018/6/15.
 */

public class CommonConfig {
    /***是否调试***/
    private static final boolean IS_DEBUG = true;
    /***APP_ID***/
    private static final String PACKAGE_NAME = "com.xy.demo";
    /***APP_Name***/
    private static final String APP_NAME = "Demo";
    /***正式环境Local_host***/
    private static final String LOCAL_HOST = "https://api.qingyunfu.cn/device/";
    /***测试环境Local_host***/
    private static final String TEST_LOCAL_HOST = "https://api.qingyunfu.cn/device/";

    public static final String IMG_URL_PATH = getHost() + "nurserys/readImgFid?fid=";

    /*H5的Host*/
    private static final String WEBPAGE_HOST = "";
    private static final String WEBPAGE_HOST_TEST = "";

    /*****sp默认文件名*****/
    public static final String SP_DEFAULT_NAME = "xy_demo";
    /*******TOKEN******/
    public volatile static String SP_TOKEN_NAME = "aliahganl2y8hbslks";
    /*******PHONE******/
    public static String SP_PHONE = "";
    /*******userID******/
    public static int SP_UUID = 0;
    /*******姓名******/
    public static String SP_NAME = "";
    //用户登录名
    public static String SP_USERNAME = "";
    //用户密码
    public static String SP_PASSWORD = "";
    /*******登录状态******/
    public static boolean IS_LOGIN = false;
    /***正式环境TCP_host***/
    private static final String TCP_HOST = "dev.qingyunfu.cn";

    /**
     * 获取当前是否调试
     */
    public static boolean isDebug() {
        return IS_DEBUG;
    }

    /**
     * 根据是否调试返回地址
     */
    public static String getHost() {
        if (!IS_DEBUG) {
            LogUtils.e("Address", LOCAL_HOST);
            return LOCAL_HOST;
        } else {
            LogUtils.e("Address", TEST_LOCAL_HOST);
            return TEST_LOCAL_HOST;
        }
    }

    /**
     * 获取网络连接异常请求的地址
     */
    public static String getWebpageHost() {
        if (!IS_DEBUG) {
            return WEBPAGE_HOST;
        } else {
            return WEBPAGE_HOST_TEST;
        }
    }

    /*获取APP_ID*/
    public static String getPackageName() {
        return PACKAGE_NAME;
    }

    /*获取APP_NAME*/
    public static String getAppName() {
        return APP_NAME;
    }

    public static boolean isLogin() {
        return IS_LOGIN;
    }

    /**
     * 获取网络连接异常请求的地址
     */
    public static String getTcpHost() {
        return TCP_HOST;
    }

}
