package com.xy.demo.network;

/**
 * Created by 1 on 2017/10/17.
 */

public class HttpResponse<T> {

    private boolean success;
    private String msg;
    private int code;
    private T data;

    public HttpResponse(Object data) {
        this.data = (T) data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "success=" + success + ", msg=" + msg + ", data=" + data;
    }
}
