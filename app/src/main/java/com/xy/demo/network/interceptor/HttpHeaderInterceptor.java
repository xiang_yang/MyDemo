package com.xy.demo.network.interceptor;

import com.xy.demo.config.CommonConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by xy on 2017/11/24.
 */

public class HttpHeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request()
                .newBuilder()
                .addHeader("securityKey", "aliahganl2y8hbslks")
                .build();
        return chain.proceed(request);
    }


}
