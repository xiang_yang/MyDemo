package com.xy.demo.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class DiffEquipmentLogin implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        if (isDiffEquipmentLogin(response)) {
            //跳转登录页面

        }
        return response;
    }

    private boolean isDiffEquipmentLogin(Response response) {
        //异设备登录返回的CODE
        return response.code() == 401;
    }
}
