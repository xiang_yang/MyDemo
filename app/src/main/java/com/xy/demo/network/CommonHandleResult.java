package com.xy.demo.network;

import com.xy.demo.bean.NullBean;
import com.xy.demo.util.LogUtils;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.FlowableTransformer;
import io.reactivex.functions.Function;

/**
 * Created by xy on 2018/10/17.
 */

public class CommonHandleResult {

    public static <T> FlowableTransformer<HttpResponse<T>, T> handleReuslt() {
        return new FlowableTransformer<HttpResponse<T>, T>() {
            @Override
            public Flowable<T> apply(Flowable<HttpResponse<T>> httpResponseFlowable) {
                return httpResponseFlowable.flatMap(new Function<HttpResponse<T>, Flowable<T>>() {
                    @Override
                    public Flowable<T> apply(HttpResponse<T> tHttpResponse) {
                        if (tHttpResponse.getCode() == 200) {
                            if (tHttpResponse.getData() == null) {
                                return createData(new HttpResponse<T>(new NullBean()).getData());
                            }
                            return createData(tHttpResponse.getData());
                        } else {
                            LogUtils.e("_ERROR", tHttpResponse.toString());
                            return Flowable.error(new ApiException(tHttpResponse.getMsg(), tHttpResponse.isSuccess()));

                        }
                    }
                });
            }
        };
    }


    public static <T> Flowable<T> createData(final T t) {
        return Flowable.create(new FlowableOnSubscribe<T>() {
            @Override
            public void subscribe(FlowableEmitter<T> emitter) throws Exception {
                try {
                    emitter.onNext(t);
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                    LogUtils.e("网络请求处理错误", e.getMessage());
                }
            }
        }, BackpressureStrategy.BUFFER);
    }

}
