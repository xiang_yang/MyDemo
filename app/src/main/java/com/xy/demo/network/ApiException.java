package com.xy.demo.network;

/**
 * Created by song on 2017/12/6.
 */

public class ApiException extends Exception {

    private boolean success;

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, boolean success) {
        super(message);
        this.success = success;
    }

    public boolean getStatus() {
        return success;
    }

    public void setStatus(boolean success) {
        this.success = success;
    }
}
