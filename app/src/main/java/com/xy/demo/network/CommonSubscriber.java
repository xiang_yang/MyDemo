package com.xy.demo.network;

import com.xy.demo.util.LogUtils;
import com.xy.demo.util.ToastUtils;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by song on 2017/12/6.
 */

public abstract class CommonSubscriber<T> extends ResourceSubscriber<T> {
    private BaseView mView;
    private String mErrorMsg;
//    private static String fromServer = "****";

    public CommonSubscriber() {
    }

    public CommonSubscriber(String mErrorMsg) {
        this.mErrorMsg = mErrorMsg;
    }

    public CommonSubscriber(BaseView mView) {
        this.mView = mView;
    }

    public CommonSubscriber(BaseView mView, String mErrorMsg) {
        this.mView = mView;
        this.mErrorMsg = mErrorMsg;
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(Throwable t) {
        LogUtils.e("数据加载失败", t.getMessage());
        if (mView != null) {
            mView.stateError();
        }
//        if(t instanceof ApiException){
//            ToastUtils.showShort("数据加载失败,请重试");
//        }

        //网络连接不通不上触发
        onError();
    }

    public void onError() {
        LogUtils.e("网络异常", "请检查您的网络");
        ToastUtils.showShort("请检查您的网络");
    }

}
