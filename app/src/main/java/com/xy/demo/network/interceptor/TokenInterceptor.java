package com.xy.demo.network.interceptor;


import com.xy.demo.util.LogUtils;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        //根据和服务端的约定判断token过期
        if (isTokenExpired(response)) {
            //同步请求方式，获取最新的Token
            String newToken = getNewToken();
            //使用新的Token，创建新的请求
            Request newRequest = chain.request()
                    .newBuilder()
                    .header("token", newToken)
                    .build();
            //重新请求
            return chain.proceed(newRequest);
        }
        return response;
    }

    /**
     * 根据Response，判断Token是否失效
     *
     * @param response
     * @return
     */
    private boolean isTokenExpired(Response response) {
        //token失效code编码
        return response.code() == 401;
    }

    /**
     * 同步请求方式，获取最新的Token
     *
     * @return
     */
    private String getNewToken() throws IOException {
        Lock lock = new ReentrantLock();
        try {
            //重入锁防止同时获取token
            lock.lock();
            //请求接口
        } catch (Exception e) {
            LogUtils.e(e.getMessage());
        } finally {
            lock.unlock();
        }
        // 通过sp持久化缓存的账号密码进行重新登录获取token
        //登录时更换一次token或者访问服务器延长token的时间

        return "";
    }
}
