package com.xy.demo.network;

/**
 * Created by xy on 2017/10/16.
 * presenter基类
 */

public interface BasePresenter<T extends BaseView> {

    void attachView(T view);

    void detachView();

}
