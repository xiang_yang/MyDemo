package com.xy.demo.bean;

public class TcpRequestBean implements java.io.Serializable {

    private String packStartFlag = "@B#@,";

    private String deviceType;

    private String deviceVersion;

    private String requestType;

    private String deviceUniqueNumber;

    private String requestUniqueNumber;

    private String data;

    private String packEndFlag = ",@E#@";

    public String getPackStartFlag() {
        return packStartFlag;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getDeviceUniqueNumber() {
        return deviceUniqueNumber;
    }

    public void setDeviceUniqueNumber(String deviceUniqueNumber) {
        this.deviceUniqueNumber = deviceUniqueNumber;
    }

    public String getRequestUniqueNumber() {
        return requestUniqueNumber;
    }

    public void setRequestUniqueNumber(String requestUniqueNumber) {
        this.requestUniqueNumber = requestUniqueNumber;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPackEndFlag() {
        return packEndFlag;
    }
}
