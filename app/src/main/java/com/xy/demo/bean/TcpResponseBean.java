package com.xy.demo.bean;

public class TcpResponseBean implements java.io.Serializable {

    private String packStartFlag;

    private String responseType;

    private String requestUniqueNumber;

    private String data;

    private String packEndFlag;

    public String getPackStartFlag() {
        return packStartFlag;
    }

    public void setPackStartFlag(String packStartFlag) {
        this.packStartFlag = packStartFlag;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getRequestUniqueNumber() {
        return requestUniqueNumber;
    }

    public void setRequestUniqueNumber(String requestUniqueNumber) {
        this.requestUniqueNumber = requestUniqueNumber;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPackEndFlag() {
        return packEndFlag;
    }

    public void setPackEndFlag(String packEndFlag) {
        this.packEndFlag = packEndFlag;
    }
}
