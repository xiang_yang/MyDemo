package com.xy.demo.bean;

public class LoginDataBean {

    /**
     * user : {"userID":20,"uname":"super","name":"SUPER","uphone":"16666666666","usite":"DEV","udanwei":"DEV","uremark":"DEV","roleId":9999,"updateDel":1}
     * token : 7f7a96-7f1f-461d-b8ac-926c288dd756
     */

    private UserBean user;
    private String token;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class UserBean {
        /**
         * userID : 20
         * uname : super
         * name : SUPER
         * uphone : 16666666666
         * usite : DEV
         * udanwei : DEV
         * uremark : DEV
         * roleId : 9999
         * updateDel : 1
         */

        private int userID;
        private String uname;
        private String name;
        private String uphone;
        private String usite;
        private String udanwei;
        private String uremark;
        private int roleId;
        private int updateDel;

        public int getUserID() {
            return userID;
        }

        public void setUserID(int userID) {
            this.userID = userID;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUphone() {
            return uphone;
        }

        public void setUphone(String uphone) {
            this.uphone = uphone;
        }

        public String getUsite() {
            return usite;
        }

        public void setUsite(String usite) {
            this.usite = usite;
        }

        public String getUdanwei() {
            return udanwei;
        }

        public void setUdanwei(String udanwei) {
            this.udanwei = udanwei;
        }

        public String getUremark() {
            return uremark;
        }

        public void setUremark(String uremark) {
            this.uremark = uremark;
        }

        public int getRoleId() {
            return roleId;
        }

        public void setRoleId(int roleId) {
            this.roleId = roleId;
        }

        public int getUpdateDel() {
            return updateDel;
        }

        public void setUpdateDel(int updateDel) {
            this.updateDel = updateDel;
        }
    }
}
