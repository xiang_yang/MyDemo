package com.xy.demo.util;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

public class SearchUtil {
    private static StringBuffer result;

    public static String getMainText(String allText, String mainText) {
        result = new StringBuffer();
        getPosition(allText, mainText);
        return result.toString();
    }

    public static SpannableString getTextSpannable(String allText, String mainText, boolean isFirst) {
        String allTextPingYin = MyPinyinHelper.toPinYin(allText);
        SpannableString spannableString = new SpannableString(allText);
        result = new StringBuffer();
        if (isFirst) {
            getPositionFirst(allText, mainText);
        } else {
            getPosition(allText, mainText);
        }
        if (allText.toLowerCase().contains(mainText)) {
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#20B262")), allText.toLowerCase().indexOf(mainText)
                    , allText.toLowerCase().indexOf(mainText) + String.valueOf(mainText).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        } else if (allTextPingYin.contains(mainText)) {
            getPositionFirst(allText, mainText);
            int changeText = allText.indexOf(result.toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#20B262"))
                    , changeText, changeText + result.toString().length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);


        }
        return spannableString;
    }

    public static String getMainFirstText(String allText, String mainText) {
        result = new StringBuffer();
        getPositionFirst(allText, mainText);
        return result.toString();
    }

    //张三李四zhangsanlisi          sanl
    private static void getPosition(String allText, String mainText) {
        String allTextPingYin = MyPinyinHelper.toPinYin(allText);//zhangsanlisi     lisi
        int textPosition = allTextPingYin.indexOf(mainText);
        String mainFirst = mainText.substring(0, 1);
        if (textPosition != -1) {
            //如果包含该字符串
            for (int i = 0; i < allText.length(); i++) {
                String tempChar = allText.substring(i, i + 1);
                String tempPinyin = MyPinyinHelper.toPinYin(tempChar);
                int length = tempPinyin.length();
                if (tempPinyin.contains(mainFirst)) {
                    result.append(tempChar);
                    String substring = allText.substring(i + 1);
                    String substring1;
                    if (mainText.length() > length) {
                        substring1 = mainText.substring(length);
                        if (substring1.length() > 0) {
                            getPosition(substring, substring1);
                        }
                    }
                    break;
                }
            }
        }
    }

    //张三李四zhangsanlisi          sanl
    private static void getPositionFirst(String allText, String mainText) {
        String allTextPingYin = MyPinyinHelper.toPinYin(allText);//zhangsanlisi     lisi
        int textPosition = allTextPingYin.indexOf(mainText);
        String mainFirst = mainText.substring(0, 1);
        if (textPosition != -1) {
            //如果包含该字符串
            for (int i = 0; i < allText.length(); i++) {
                String tempChar = allText.substring(i, i + 1);
                String tempPinyin = MyPinyinHelper.toPinYin(tempChar);
                int length = tempPinyin.length();
                if (tempPinyin.substring(0, 1).equals(mainFirst)) {
                    result.append(tempChar);
                    String substring = allText.substring(i + 1);
                    String substring1;
                    if (mainText.length() > length) {
                        substring1 = mainText.substring(length);
                        if (substring1.length() > 0) {
                            getPositionFirst(substring, substring1);
                        }
                    }
                    break;
                }
            }
        }
    }
}
