package com.xy.demo.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.xy.demo.R;

/**
 * Glide.with(context).resumeRequests()
 * Glide.with(context).pauseRequests()
 * 当列表在滑动的时候，调用pauseRequests()取消请求，滑动停止时，调用resumeRequests()恢复请求
 * <p>
 * Glide.clear()
 * 清除缓存
 */
public class GlideUtil {
    public static void loadImage(Context context, String imgUrl, ImageView imageView) {
        LogUtils.i("图片加载中", imgUrl);
        Glide.with(context)
                .load(imgUrl)
//                .placeholder(R.mipmap.loading)//加载中等待图片
                .error(R.mipmap.img_error)//加载失败显示图片
//                .asGif()//显示GIT播放
                .skipMemoryCache(true)//跳过内存缓存
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)//设置外存缓存模式    DiskCacheStrategy.NONE 什么都不缓存
//                                                                              DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//                                                                              DiskCacheStrategy.RESULT 仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//                                                                              DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）
                .into(imageView);

    }
    /**
     * "file:///android_asset/sample.gif"
     * @param context
     * @param imgUrl
     * @param imageView
     */
    public static void loadGifImage(Context context, String imgUrl, ImageView imageView) {
        Glide.with(context)
                .load(imgUrl)
//                .placeholder(R.mipmap.loading)//加载中等待图片
                .asGif()//显示GIT播放
                .skipMemoryCache(true)//跳过内存缓存
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)//设置外存缓存模式    DiskCacheStrategy.NONE 什么都不缓存
//                                                                              DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//                                                                              DiskCacheStrategy.RESULT 仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//                                                                              DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）
                .into(imageView);

    }

    public static void loadDoctorImage(Context context, String imgUrl, ImageView imageView) {
        LogUtils.i("图片加载中", imgUrl);
        Glide.with(context)
                .load(imgUrl)
//                .placeholder(R.mipmap.loading)//加载中等待图片
                .error(R.mipmap.img_error)//加载失败显示图片
//                .asGif()//显示GIT播放
                .skipMemoryCache(true)//跳过内存缓存
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)//设置外存缓存模式    DiskCacheStrategy.NONE 什么都不缓存
//                                                                              DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//                                                                              DiskCacheStrategy.RESULT 仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//                                                                              DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）
                .into(imageView);

    }

    //加载圆角图片
    public static void loadFilletImage(final Context context, String imgUrl, final ImageView imageView) {
        LogUtils.i("图片加载中", imgUrl);
        Glide.with(context)
                .load(imgUrl)
                .bitmapTransform(new GlideRoundTransform(context))      //可添加参数自行设置圆角弧度
//                .placeholder(R.mipmap.loading)//加载中等待图片
                .error(R.mipmap.img_error)//加载失败显示图片
//                .asGif()//显示GIT播放
                .skipMemoryCache(true)//跳过内存缓存
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)//设置外存缓存模式    DiskCacheStrategy.NONE 什么都不缓存
//                                                                              DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//                                                                              DiskCacheStrategy.RESULT 仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//                                                                              DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）
                .into(imageView);

    }

    //加载圆形图片
    public static void loadRoundedImage(final Context context, String imgUrl, final ImageView imageView) {
        LogUtils.i("图片加载中", imgUrl);
        Glide.with(context)
                .load(imgUrl)
                .bitmapTransform(new CircleTransform(context))      //圆形加载图片
//                .placeholder(R.mipmap.loading)//加载中等待图片
                .error(R.mipmap.img_error)//加载失败显示图片
//                .asGif()//显示GIT播放
                .skipMemoryCache(true)//跳过内存缓存
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)//设置外存缓存模式    DiskCacheStrategy.NONE 什么都不缓存
//                                                                              DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//                                                                              DiskCacheStrategy.RESULT 仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//                                                                              DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）
                .into(imageView);

    }

    //圆形加载
    public static class CircleTransform extends BitmapTransformation {
        public CircleTransform(Context context) {
            super(context);
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return circleCrop(pool, toTransform);
        }

        private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
            if (source == null) return null;

            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            // TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            return result;
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }

    //圆角加载
    public static class GlideRoundTransform extends BitmapTransformation {
        private static float radius = 0f;

        public GlideRoundTransform(Context context) {
            this(context, 4);
        }

        public GlideRoundTransform(Context context, int dp) {
            super(context);
            this.radius = Resources.getSystem().getDisplayMetrics().density * dp;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return roundCrop(pool, toTransform);
        }

        private static Bitmap roundCrop(BitmapPool pool, Bitmap source) {
            if (source == null) return null;

            Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
            canvas.drawRoundRect(rectF, radius, radius, paint);
            return result;
        }

        @Override
        public String getId() {
            return getClass().getName() + Math.round(radius);
        }
    }
}
