package com.xy.demo.util.popup;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.xy.demo.R;

import java.util.List;


public class ListPopWinAdapter extends RecyclerView.Adapter<ListPopWinAdapter.MyViewHolder> {

    private List<String> list;//存放数据
    private Context context;

    public ListPopWinAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ListPopWinAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.pop_active_picker_item, null, false));
        holder.tv_pop=parent.findViewById(R.id.tv_pop);
        return holder;
    }

    /**
     * 在这里可以获得每个子项里面的控件的实例，比如这里的TextView,子项本身的实例是itemView，
     * 在这里对获取对象进行操作
     * holder.itemView是子项视图的实例，holder.textView是子项内控件的实例
     * position是点击位置
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final ListPopWinAdapter.MyViewHolder holder, final int position) {
        holder.tv_pop.setText(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickListener.onClick(list.get(position),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //这里定义的是子项的类，不要在这里直接对获取对象进行操作
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_pop;

        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }




    /*之下的方法都是为了方便操作，并不是必须的*/


    //去除指定位置的子项
    public boolean removeItem(int position) {
        if (position < list.size() && position >= 0) {
            list.remove(position);
            notifyItemRemoved(position);
            return true;
        }
        return false;
    }

    //清空显示数据
    public void clearAll() {
        list.clear();
        notifyDataSetChanged();
    }

    private SetOnClickListener setOnClickListener;

    public interface SetOnClickListener {//创建一个接口类
        void onClick(String result, int position);//创建一个回调函数，实例化接口的时候就要具体化这个回调函数，即要有函数体
    }
    public void SetOnClickListener(SetOnClickListener setOnClickListener){
        this.setOnClickListener = setOnClickListener;
    }
}
