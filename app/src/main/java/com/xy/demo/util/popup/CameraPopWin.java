package com.xy.demo.util.popup;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xy.demo.R;
import com.xy.demo.util.LogUtils;


public class CameraPopWin extends PopupWindow implements View.OnClickListener {
    //popwin容器
    private View contentView;
    //取消按钮
    private TextView tvCancel, tvAlbum, tvCamera;
    //空白区域点击隐藏
    private View view;
    //popwin弹出窗体
    private View pickerContainer;


    private Context mContext;
    private Long preTime = 0L;


    public CameraPopWin(Builder builder) {
        this.mContext = builder.context;
        this.pickerListener = builder.pickerListener;
        initView();
    }

    /**
     * 创建popwin
     */
    public static class Builder {
        private Context context;
        private OnItemPickerListener pickerListener;

        public Builder(Context context, OnItemPickerListener pickerListener) {
            this.context = context;
            this.pickerListener = pickerListener;
        }

        public CameraPopWin build() {
            return new CameraPopWin(this);
        }
    }


    private void initView() {
        contentView = LayoutInflater.from(mContext).inflate(R.layout.pop_camera_picker, null);
        tvCancel = contentView.findViewById(R.id.tv_pop_cancel);
        tvAlbum = contentView.findViewById(R.id.tv_pop_album);
        tvCamera = contentView.findViewById(R.id.tv_pop_camera);
        pickerContainer = contentView.findViewById(R.id.ll_popWin);
        view = contentView.findViewById(R.id.view);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvAlbum.setOnClickListener(this);
        contentView.setOnClickListener(this);
        view.setOnClickListener(this);

        setTouchable(true);
        setFocusable(true);
        setAnimationStyle(R.style.AnimPopWin);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        //全屏显示
        setClippingEnabled(false);
    }

    /**
     * 弹出pop
     *
     * @param activity
     */
    public void showPopWin(Activity activity) {
        if (null != activity) {
            TranslateAnimation trans = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0);
            showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
            trans.setDuration(400);
            trans.setInterpolator(new AccelerateDecelerateInterpolator());
            pickerContainer.startAnimation(trans);
        }
    }

    /**
     * 关闭pop
     */
    public void dismissPopWin() {
        Long currentTime = System.currentTimeMillis();
        LogUtils.e("取消", currentTime + "=" + preTime + "=" + (currentTime - preTime));
        if (currentTime - preTime <= 400) {
            return;
        } else {
            preTime = currentTime;
        }
        TranslateAnimation trans = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);
        trans.setDuration(400);
        trans.setInterpolator(new AccelerateDecelerateInterpolator());
        trans.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        pickerContainer.startAnimation(trans);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_popWin:
                dismissPopWin();
                break;
            case R.id.tv_pop_camera:
                pickerListener.onCameraPickerCompleted();
                dismissPopWin();
                break;
            case R.id.tv_pop_album:
                pickerListener.onAlbumPickerCompleted();
                dismissPopWin();
                break;
            case R.id.tv_pop_cancel:
                dismissPopWin();
                break;
            case R.id.view:
                dismissPopWin();
                break;
            default:
                break;
        }
    }

    private OnItemPickerListener pickerListener;

    public interface OnItemPickerListener {
        void onCameraPickerCompleted();

        void onAlbumPickerCompleted();
    }

}
