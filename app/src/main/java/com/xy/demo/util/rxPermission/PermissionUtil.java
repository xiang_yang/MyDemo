package com.xy.demo.util.rxPermission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;


public class PermissionUtil {

    /**
     * 多权限检查方法
     *
     * @param context
     * @param permissions 多权限
     * @return 所有请求都通过，返回true
     */
    public static boolean checkPermission(Context context, String... permissions) {
        for (String per :
                permissions) {
            if (!isGranted(context, per)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 权限是否获取成功
     *
     * @param context
     * @param permission 权限字符串
     * @return true，获取成功
     */
    public static boolean isGranted(Context context, String permission) {
        int per = ActivityCompat.checkSelfPermission(context, permission);
        return per == PermissionChecker.PERMISSION_GRANTED;
    }

    /**
     * 权限申请
     *
     * @param activity
     * @param permission  权限字符串
     * @param requestCode 请求码，标识权限申请
     */
    public static void requestPermission(Activity activity, String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
    }

    /**
     * 多权限申请
     *
     * @param activity
     * @param requestCode 请求码，标识权限申请
     * @param permissions 多权限，字符数组
     */
    public static void requestPermissions(Activity activity, int requestCode, String... permissions) {
//        List<String> mPermiss = new ArrayList<>();
//        for (String permission : permissions) {
//            if (!isGranted(activity, permission)) {
//                mPermiss.add(permission);
//            }
//        }
//        String[] requests = new String[mPermiss.size()];
//        for (int i = 0; i < mPermiss.size(); i++) {
//            requests[i] = mPermiss.get(i);
//        }
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }
    public static String[] getRequiredPermissions() {
        return new String[]{
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.BLUETOOTH};
    }
//    public static boolean shouldShowRequestPermissionRationale(final Activity context, final String permission, String reason,
//                                                               final DialogInterface.OnClickListener okListener, final DialogInterface.OnClickListener cancleListener,
//                                                               final int requestCode) {
//        if (!ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
//            DialogHelper.showMessageDialog(context,reason, context.getString(R.string.cancel), context,getString(R.string.delete), new DialogHelper.OnDialogCallBack() {
//                @Override
//                public void onAction(String passWord) {
//                    deletePic();
//                }
//            });
//            AlertUtil.showMessageOkCancle(context, reason, "确定", "取消", okListener, cancleListener);
//            return false;
//        } else {
//            return true;
//        }
//    }
}
