package com.xy.demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SimpleDataFormat工具类
 *
 * @author xy
 * @date 2018/10/23
 */
public class SimpleDataUtil {
    private static SimpleDateFormat sdf;

    public static String getSimpleData(Date date) {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String getSimpleData(long date) {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date(date));
    }

    public static String getChinaData(Date date) {
        sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return sdf.format(date);
    }

    public static String getChinaData(long date) {
        sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return sdf.format(new Date(date));
    }

    public static String getInfoData(Date date) {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date);
    }

    public static String getInfoData(long date) {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(new Date(date));
    }

    public static String getWeekData(Date date) {
        sdf = new SimpleDateFormat("yyyy年MM月dd日 EEEE");
        return sdf.format(date);
    }

    public static String getWeekData(long date) {
        sdf = new SimpleDateFormat("yyyy年MM月dd日 EEEE");
        return sdf.format(new Date(date));
    }


}
