package com.xy.demo.util.dialogFragment;

import androidx.annotation.FloatRange;
import androidx.annotation.LayoutRes;
import androidx.annotation.StyleRes;

/**
 * 公用样式Dialog
 */
public class CommonDialog extends BaseDialog implements ViewConvertListener {
    private ViewConvertListener convertListener;

    public static CommonDialog newInstance() {
        CommonDialog dialog = new CommonDialog();
        return dialog;
    }

    /**
     * 设置Dialog布局
     *
     * @param layoutId
     * @return
     */
    public CommonDialog setLayoutId(@LayoutRes int layoutId) {
        this.mLayoutResId = layoutId;
        return this;
    }

    public CommonDialog setDialogAnimationRes(int id){
        setAnimStyle(id);
        return this;
    }

    public CommonDialog setGravity(int gravity){
        mShowGravity=gravity;
        return this;
    }

    public CommonDialog setWidthAspect(float aspect){
        widthAspect=aspect;
        return this;
    }

    public CommonDialog setHeightAspect(float mHeightAspect) {
        heightAspect = mHeightAspect;
        return this;
    }

    /**
     * 设置进入退出动画
     *
     * @param animStyle
     * @return
     */
    public CommonDialog setAnimStyle(@StyleRes int animStyle) {
        mAnimStyle = animStyle;
        return this;
    }

    /**
     * 设置背景昏暗度
     *
     * @param dimAmount
     * @return
     */
    public CommonDialog setDimAmout(@FloatRange(from = 0, to = 1) float dimAmount) {
        mDimAmount = dimAmount;
        return this;
    }
    /**
     * 设置是否点击外部取消
     *
     * @param outCancel
     * @return
     */
    public CommonDialog setOutCancel(boolean outCancel) {
        mOutCancel = outCancel;
        return this;
    }

    @Override
    public int setUpLayoutId() {
        return mLayoutResId;
    }

    @Override
    public void convertView(ViewHolder holder, BaseDialog dialog) {
        if (convertListener != null) {
            convertListener.convertView(holder, dialog);
        }
    }

    public CommonDialog setConvertListener(ViewConvertListener convertListener) {
        this.convertListener = convertListener;
        return this;
    }
}