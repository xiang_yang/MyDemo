package com.xy.demo.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.DisplayMetrics;
import android.util.Log;


import java.util.Locale;


public class LanguageUtil {
    public static Context appContext = Utils.getApp();

    /**
     * App 语言切换成英文
     **/
    public static void appLanguageSwitchToEnglish() {
        Locale locale = new Locale("en", "US");
        //强制修改成英文
        LanguageUtil.changeAppLanguage(locale, true);
//        ToastShow.toastShow(MyApplication.getContext(),"Current App Language Have Switch To English");
    }

    /**
     * App语言切换成中文
     */
    public static void appLanguageSwitchToChinese() {
        Locale chineseLocale = new Locale("zh", "CN");
        //强制切换成中文简体
        LanguageUtil.changeAppLanguage(chineseLocale, true);
//        ToastShow.toastShow(MyApplication.getContext(),"当前App语言已切换为简体中文");
    }

    public static void restartApp(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (null == packageManager) {
            Log.e("restartApp_ERROR", "null == packageManager");
            return;
        }
        final Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    /**
     * 更改应用语言
     *
     * @param locale      语言地区
     * @param persistence 是否持久化
     */

    public static void changeAppLanguage(Locale locale, boolean persistence) {
        //获取应用程序的所有资源对象
        Resources resources = appContext.getResources();
        //获取屏幕参数
        DisplayMetrics metrics = resources.getDisplayMetrics();
        //获取设置对象
        Configuration configuration = resources.getConfiguration();
        //如果API < 17
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.locale = locale;
        } else //如果 17 < = API < 25 Android 7.7.1
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
                configuration.setLocale(locale);
            } else {//API 25  Android 7.7.1
                configuration.setLocale(locale);
                configuration.setLocales(new LocaleList(locale));
            }
        //更新配置信息
        resources.updateConfiguration(configuration, metrics);
        //持久化
        if (persistence) {
            SharePreferenceHelper sharePreferenceHelper = Utils.getSharePreferenceHelper();
            sharePreferenceHelper.put("LANGUAGE", locale.getLanguage());
            restartApp(appContext);
        }

    }

    /**
     * 获取App当前语言
     * 如果存过信息那么以存的信息为准否则取系统的配置信息为准
     *
     * @Return Locale
     **/
    public static Locale getAppLocale() {
        SharePreferenceHelper sharePreferenceHelper = Utils.getSharePreferenceHelper();
        //读取本地配置文件信息
        Locale currentAppLocale = null;
        currentAppLocale = (Locale) sharePreferenceHelper.get("LANGUAGE", new Locale("zh", "CN"));
        if (currentAppLocale == null) {
            currentAppLocale = getSystemLocale();
        }
        return currentAppLocale;
    }

    /******
     * 获取当前系统语言
     */
    public static Locale getSystemLocale() {
        Locale currentSystemLocale;
        //API >= 24
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            currentSystemLocale = Resources.getSystem().getConfiguration().getLocales().get(0);
        } else {
            currentSystemLocale = Resources.getSystem().getConfiguration().locale;
        }
        return currentSystemLocale;
    }
}
