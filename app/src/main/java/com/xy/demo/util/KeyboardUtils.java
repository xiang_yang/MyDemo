package com.xy.demo.util;

import android.app.Activity;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

/**
 KeyboardUtils.getKeyboardStateObserver(this).
 setKeyboardVisibilityListener(new KeyboardUtils.OnKeyboardVisibilityListener() {
@Override
public void onKeyboardShow() {
//                        linearLayout.setVisibility(View.GONE);
ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout.getLayoutParams();
lp.topMargin = DensityUtil.dip2px(64.0f);
constraintLayout.setLayoutParams(lp);
ToolLog.e("LoginActivity", "键盘弹出");
}

@Override
public void onKeyboardHide() {
//                        linearLayout.setVisibility(View.VISIBLE);
ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout.getLayoutParams();
lp.topMargin = DensityUtil.dip2px(143.0f);
constraintLayout.setLayoutParams(lp);
ToolLog.e("LoginActivity", "键盘收回");
}
});
 */
public class KeyboardUtils {

    private static final String TAG = KeyboardUtils.class.getSimpleName();

    public static KeyboardUtils getKeyboardStateObserver(Activity activity) {
        return new KeyboardUtils(activity);
    }

    private View mChildOfContent;
    private int usableHeightPrevious;
    private OnKeyboardVisibilityListener listener;

    public void setKeyboardVisibilityListener(OnKeyboardVisibilityListener listener) {
        this.listener = listener;
    }

    private KeyboardUtils(Activity activity) {
        FrameLayout content = (FrameLayout) activity.findViewById(android.R.id.content);
        mChildOfContent = content.getChildAt(0);
        mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                possiblyResizeChildOfContent();
            }
        });
    }

    private void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight();
        if (usableHeightNow != usableHeightPrevious) {
            int usableHeightSansKeyboard = mChildOfContent.getRootView().getHeight();
            int heightDifference = usableHeightSansKeyboard - usableHeightNow;
            if (heightDifference > (usableHeightSansKeyboard / 3)) {
                if (listener != null) {
                    listener.onKeyboardShow();
                }
            } else {
                if (listener != null) {
                    listener.onKeyboardHide();
                }
            }
            usableHeightPrevious = usableHeightNow;
            Log.d(TAG, "usableHeightNow: " + usableHeightNow + " | usableHeightSansKeyboard:" + usableHeightSansKeyboard + " | heightDifference:" + heightDifference);
        }
    }

    private int computeUsableHeight() {
        Rect r = new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);

        Log.d(TAG, "rec bottom>" + r.bottom + " | rec top>" + r.top);
        return (r.bottom - r.top);// 全屏模式下： return r.bottom
    }

    public interface OnKeyboardVisibilityListener {
        void onKeyboardShow();

        void onKeyboardHide();
    }
}
