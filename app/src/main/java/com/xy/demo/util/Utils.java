package com.xy.demo.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;

import com.xy.demo.R;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

/**
 * <pre>
 *     author: Blankj
 *     blog  : http://blankj.com
 *     time  : 16/12/08
 *     desc  : Utils初始化相关
 */
public final class Utils {
    private static SharePreferenceHelper sharePreferenceHelper;
    private static Application sApplication;
    private static ACache aCache;
    private static float scale;
    static WeakReference<Activity> sTopActivityWeakRef;
    static List<Activity> sActivityList = new LinkedList<>();

    private static Application.ActivityLifecycleCallbacks mCallbacks = new Application.ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            sActivityList.add(activity);
            setTopActivityWeakRef(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {
            setTopActivityWeakRef(activity);
        }

        @Override
        public void onActivityResumed(Activity activity) {
            setTopActivityWeakRef(activity);
        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            sActivityList.remove(activity);
        }
    };

    private Utils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 初始化工具类
     *
     * @param app 应用
     */
    public static void init(@NonNull final Application app) {
        Utils.sApplication = app;
        app.registerActivityLifecycleCallbacks(mCallbacks);
        //初始化ASimpleCache
        aCache = ACache.get(Utils.getApp());
        setScale(Utils.getApp().getResources().getDisplayMetrics().density);
        sharePreferenceHelper = new SharePreferenceHelper(sApplication);
    }

    /**
     * 获取Application
     *
     * @return Application
     */
    public static Application getApp() {
        if (sApplication != null) return sApplication;
        throw new NullPointerException("u should init first");
    }

    public static ACache getACache() {
        if (sApplication != null) return aCache;
        throw new NullPointerException("u should init first");
    }

    private static void setTopActivityWeakRef(Activity activity) {
        if (sTopActivityWeakRef == null || !activity.equals(sTopActivityWeakRef.get())) {
            sTopActivityWeakRef = new WeakReference<>(activity);
        }
    }

    public static float getScale() {
        return scale;
    }

    public static void setScale(float scal) {
        scale = scal;
    }

    public static SharePreferenceHelper getSharePreferenceHelper() {
        return sharePreferenceHelper;
    }

    public static void finishThis(Context context, Activity activity) {
        //finish的唯一有效动画
        ActivityOptionsCompat compat = ActivityOptionsCompat.
                makeCustomAnimation(context, R.anim.start_enter_alpha, R.anim.start_exit_alpha);
        activity.finish();
    }
}
