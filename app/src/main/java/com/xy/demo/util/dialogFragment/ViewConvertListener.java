package com.xy.demo.util.dialogFragment;

public interface ViewConvertListener {
    void convertView(ViewHolder holder, BaseDialog dialog);
}
