package com.xy.demo.util;


/**
 * Created by xy on 2017/11/15 0015.
 */

public class DensityUtil {

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(float dpValue) {
        return (int) (Utils.getScale() * dpValue + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(float pxValue) {
        return (int) (pxValue / Utils.getScale() + 0.5f);
    }
}
