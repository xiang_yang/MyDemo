package com.xy.demo.util;

public class StringUtil {
    private static final String CHINESE_PHONE_CHECK = "^1[0-9]{10}$";
    private static final String CHINESE_ID_CARD_CHECK = "^\\d{15}$)|(^\\d{17}([0-9]|X)$";
    private static final String CHINESE_EMAIL_CHECK = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    private static final String YN_PHONE_CHECK = "/\\b62[28]\\d{6,11}\\b/g";

    public static boolean isNotNull(String str) {
        if (str == null || str.length() <= 1) {
            return false;
        }
        return true;
    }

    public static boolean isPhoneNumber(String phone){
        return phone.matches(YN_PHONE_CHECK) ;
    }
}
