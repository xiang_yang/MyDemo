package com.xy.demo.util;


import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 拼音辅助工具类
 */

public class MyPinyinHelper {
    /**
     * 返回一个字的拼音
     *
     * @param hanzi
     * @return
     */
    public static String toPinYin(char hanzi) {
        HanyuPinyinOutputFormat hanyuPinyin = getFormater();
        String[] pinyinArray = null;
        try {
            //是否在汉字范围内
            if (hanzi >= 0x4e00 && hanzi <= 0x9fa5) {
                pinyinArray = PinyinHelper.toHanyuPinyinStringArray(hanzi, hanyuPinyin);
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        //将获取到的拼音返回
        return pinyinArray[0];
    }

    public static HanyuPinyinOutputFormat getFormater() {
        HanyuPinyinOutputFormat hanyuPinyin = new HanyuPinyinOutputFormat();
        hanyuPinyin.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        hanyuPinyin.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        hanyuPinyin.setVCharType(HanyuPinyinVCharType.WITH_V);
        return hanyuPinyin;
    }

    public static String toPinYin(String hanzi) {
        HanyuPinyinOutputFormat hanyuPinyin = getFormater();
        String[] pinyinArray = null;
        StringBuilder sb = new StringBuilder();
        if (hanzi == null || hanzi.length() == 0) {
            return "";
        }
        try {
            for (int i = 0; i < hanzi.length(); i++) {
                char c = hanzi.charAt(i);
                //是否在汉字范围内
                if (c >= 0x4e00 && c <= 0x9fa5) {
                    pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, hanyuPinyin);
                    sb.append(pinyinArray[0]);
                } else {
                    sb.append(c);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static String toPinYinFirst(String hanzi) {
        HanyuPinyinOutputFormat hanyuPinyin = getFormater();
        String[] pinyinArray = null;
        StringBuilder sb = new StringBuilder();
        if (hanzi == null || hanzi.length() == 0) {
            return "";
        }
        try {
            for (int i = 0; i < hanzi.length(); i++) {
                char c = hanzi.charAt(i);
                //是否在汉字范围内
                if (c >= 0x4e00 && c <= 0x9fa5) {
                    pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, hanyuPinyin);
                    sb.append(pinyinArray[0].charAt(0));
                } else {
                    sb.append(c);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}