package com.xy.demo.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DownloadApk implements Runnable {
    private ProgressDialog dialog;
    InputStream is;
    FileOutputStream fos;
    private String TAG;
    private String url;
    private Activity activity;

    public DownloadApk(ProgressDialog dialog, String url, Activity activity) {
        this.dialog = dialog;
        this.url = url;
        this.activity = activity;
    }

    @Override
    public void run() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().get().url(url).build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                LogUtils.e("开始下载apk----" + url);
                long contentLength = response.body().contentLength();
                //设置最大值
                dialog.setMax((int) contentLength);
                //保存到sd卡
                ApplicationInfo applicationInfo = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
                String app_cache = applicationInfo.metaData.getString("demo_cache");
                String savePath = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + File.separator + app_cache + ".apk";
                File apkFile = new File(savePath);
                fos = new FileOutputStream(apkFile);
                //获得输入流
                is = response.body().byteStream();
                //定义缓冲区大小
                byte[] bys = new byte[1024];
                int progress = 0;
                int len = -1;
                while ((len = is.read(bys)) != -1) {
                    try {
                        Thread.sleep(1);
                        fos.write(bys, 0, len);
                        fos.flush();
                        progress += len;
                        //设置进度
                        dialog.setProgress(progress);
                    } catch (InterruptedException e) {
                        ToolLog.e("安装新app失败", e.getMessage());
                    }
                }
                //下载完成,提示用户安装
                installApk(apkFile, activity);
            }
        } catch (Exception e) {
            ToolLog.e("安装新app失败", e.getMessage());
            e.printStackTrace();
        }  finally {
            //关闭io流
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is = null;
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fos = null;
            }
        }
        dialog.dismiss();
    }

    /**
     * 下载完成,提示用户安装
     */
    @SuppressLint("NewApi")
    private void installApk(File file, Activity activity) {
        //调用系统安装程序
        Intent intent = new Intent();
        //版本在7.0以上是不能直接通过uri访问的
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Uri packageURI = Uri.parse("package:" + activity.getPackageName());
            String head=activity.getPackageName()+".demo_cache";
            Uri packageURI = FileProvider.getUriForFile(activity.getApplicationContext(), head, file);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(packageURI, "application/vnd.android.package-archive");
        }else{
            intent.setDataAndType(Uri.fromFile(file),
                    "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        }
        dialog.dismiss();
        activity.startActivityForResult(intent, 0);
    }

}
