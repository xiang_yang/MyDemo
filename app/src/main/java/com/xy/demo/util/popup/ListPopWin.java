package com.xy.demo.util.popup;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xy.demo.R;

import java.util.List;


public class ListPopWin extends PopupWindow implements View.OnClickListener {
    //popwin容器
    private View contentView;
    //取消按钮
    private ImageView iv_cancel;
    private TextView tv_title;
    private RecyclerView recyclerView;
    //空白区域点击隐藏
    private View view;
    //popwin弹出窗体
    private View pickerContainer;


    private Context mContext;
    private Long preTime = 0L;
    private ListPopWinAdapter listPopWinAdapter;
    List<String> data;
    String title;


    public ListPopWin(Builder builder) {
        this.mContext = builder.context;
        this.pickerListener = builder.pickerListener;
        this.data=builder.data;
        this.title=builder.title;
        initView();
    }

    /**
     * 创建popwin
     */
    public static class Builder {
        private Context context;
        private OnItemPickerListener pickerListener;
        List<String> data;
        String title;

        public Builder(Context context, OnItemPickerListener pickerListener) {
            this.context = context;
            this.pickerListener = pickerListener;
        }

        public Builder mDatas(List<String> mDatas){
            this.data = mDatas;
            return this;
        }
        public Builder mTitle(String title){
            this.title = title;
            return this;
        }

        public ListPopWin build() {
            return new ListPopWin(this);
        }
    }


    private void initView() {
        contentView = LayoutInflater.from(mContext).inflate(R.layout.pop_active_picker, null);
        iv_cancel = contentView.findViewById(R.id.iv_cancel);
        tv_title = contentView.findViewById(R.id.tv_title);
        tv_title.setText(title);
        pickerContainer = contentView.findViewById(R.id.ll_popWin);
        recyclerView = contentView.findViewById(R.id.recyclerView);
        view = contentView.findViewById(R.id.view);

        iv_cancel.setOnClickListener(this);
        contentView.setOnClickListener(this);
        view.setOnClickListener(this);

        setTouchable(true);
        setFocusable(true);
        setAnimationStyle(R.style.AnimPopWin);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        //全屏显示
        setClippingEnabled(false);
        listPopWinAdapter = new ListPopWinAdapter(data,mContext);
        listPopWinAdapter.SetOnClickListener(new ListPopWinAdapter.SetOnClickListener() {
            @Override
            public void onClick(String result,int position) {
                pickerListener.onPickerCompleted(result,position);
                dismissPopWin();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(listPopWinAdapter);


    }

    /**
     * 弹出pop
     *
     * @param activity
     */
    public void showPopWin(Activity activity) {
        if (null != activity) {
            TranslateAnimation trans = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0);
            showAtLocation(activity.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
            trans.setDuration(400);
            trans.setInterpolator(new AccelerateDecelerateInterpolator());
            pickerContainer.startAnimation(trans);
        }
    }

    /**
     * 关闭pop
     */
    public void dismissPopWin() {
        Long currentTime = System.currentTimeMillis();
//        LogUtils.e("取消", currentTime + "=" + preTime + "=" + (currentTime - preTime));
        if (currentTime - preTime <= 400) {
            return;
        } else {
            preTime = currentTime;
        }
        TranslateAnimation trans = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);
        trans.setDuration(400);
        trans.setInterpolator(new AccelerateDecelerateInterpolator());
        trans.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        pickerContainer.startAnimation(trans);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_popWin:
            case R.id.iv_cancel:
            case R.id.view:
                dismissPopWin();
                break;
            default:
                break;
        }
    }

    private OnItemPickerListener pickerListener;

    public interface OnItemPickerListener {
        void onPickerCompleted(String result, int position);
    }

}
