package com.xy.demo.util.gapClick;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class AspectUtil {
    private static long clickGapTime = 0;

    public static final int CLICK_GAP_RESPONSE = 1200;//1200ms内不响应

    //判断是否应该执行，true执行，false不执行
    protected boolean clickGapFilter() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - clickGapTime < CLICK_GAP_RESPONSE) {
            return false;
        }
        clickGapTime = currentTimeMillis;
        return true;
    }

    @Around("execution(@GapClick * *(..))")
    public void clickGap(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if (clickGapFilter() == true) {
            proceedingJoinPoint.proceed();
        }
    }
}
