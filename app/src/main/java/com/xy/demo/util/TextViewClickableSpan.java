package com.xy.demo.util;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.xy.demo.R;

/**
 * SpannableStringBuilder spannable = new SpannableStringBuilder(getString(R.string.login_agreement));
 *         spannable.setSpan(new TextViewClickableSpan(new TextViewClickableSpan.OnTextClickListener() {
 *             @Override
 *             public void onTextClickListener() {
 *                 //跳转协议界面
 *                 WebViewActivity.startAction(LoginActivity.this, getString(R.string.perjanjian_layananPengguna),
 *                         "file:android_asset/User_Service_Agreement.html");
 *             }
 *         }), 26, 54, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
 */
public class TextViewClickableSpan extends ClickableSpan {

    private int id;

    public TextViewClickableSpan(OnTextClickListener onTextClick) {
        this.onTextClick=onTextClick;
    }

    @Override
    public void onClick(View widget) {
        //在此处理点击事件
        onTextClick.onTextClickListener();
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setColor(Utils.getApp().getResources().getColor(R.color.main)); //设置可以点击文本部分的颜色
        tp.setUnderlineText(false);//设置该文本部分是否显示超链接形式的下划线
    }

    public interface OnTextClickListener{
        void onTextClickListener();
    }

    public OnTextClickListener onTextClick;
}
