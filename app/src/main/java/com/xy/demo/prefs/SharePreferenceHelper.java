package com.xy.demo.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.xy.demo.base.BaseApplication;
import com.xy.demo.config.CommonConfig;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by 1 on 2017/10/24.
 */
public class SharePreferenceHelper {

    private BaseApplication mContext;

    public SharePreferenceHelper(BaseApplication mContext) {
        this.mContext = mContext;
    }

    private SharedPreferences.Editor getSharePreferenceEditor(String fileName) {
        return getSharePreferences(fileName).edit();
    }

    private SharedPreferences getSharePreferences(String fileName) {
        return mContext.getSharedPreferences(TextUtils.isEmpty(fileName) ? CommonConfig.SP_DEFAULT_NAME : fileName, Context.MODE_PRIVATE);
    }

    /**
     * 数据存储方法
     *
     * @param fileName 文件名，可为空有默认值
     * @param key      存储的key
     * @param object   储存的对象
     */
    public void put(String fileName, String key, Object object) {
        SharedPreferences.Editor editor = getSharePreferenceEditor(fileName);

        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }
        editor.commit();
    }

    public void put(String key, Object object) {
        this.put(null, key, object);
    }

    /**
     * 根据key取值
     *
     * @param fileName      文件名，可为空
     * @param key           键
     * @param defaultObject 默认值
     * @return
     */
    public Object get(String fileName, String key, Object defaultObject) {
        SharedPreferences sp = getSharePreferences(fileName);

        if (defaultObject instanceof String) {
            return sp.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(key, (Long) defaultObject);
        }
        return null;
    }

    public Object get(String key, Object defaultObject) {
        return this.get(null, key, defaultObject);
    }

    /**
     * 移除某个key对应的值
     *
     * @param fileName 文件名
     * @param key      key
     */
    public void remove(String fileName, String key) {
        SharedPreferences.Editor editor = getSharePreferenceEditor(fileName);
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    public void remove(String key) {
        this.remove(null, key);
    }

    /**
     * 清除所有的数据
     *
     * @param fileName 文件名
     */
    public void clear(String fileName) {
        SharedPreferences.Editor editor = getSharePreferenceEditor(fileName);
        editor.clear();
        SharedPreferencesCompat.apply(editor);
    }

    public void clear() {
        this.clear(null);
    }

    /**
     * 查询是否存在某个键
     *
     * @param fileName 文件名
     * @param key      键
     * @return
     */
    public boolean contains(String fileName, String key) {
        SharedPreferences sp = getSharePreferences(fileName);
        return sp.contains(key);
    }

    public boolean contains(String key) {
        return this.contains(null, key);
    }

    /**
     * map的形式存储数据
     *
     * @param maps
     */
    public void saveData(Map<String, String> maps) {
        this.saveData(maps, null);
    }

    public void saveData(Map<String, String> maps, String fileName) {
        if (null == maps || maps.size() == 0) return;
        SharedPreferences.Editor editor = getSharePreferenceEditor(fileName);
        for (Map.Entry<String, String> map : maps.entrySet()) {
            String key = map.getKey();
            String value = map.getValue();
            editor.putString(key, value);
        }
        editor.commit();
    }


    /**
     * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
     *
     * @author zhy
     */
    private static class SharedPreferencesCompat {
        private static final Method sApplyMethod = findApplyMethod();

        /**
         * 反射查找apply的方法
         *
         * @return
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        private static Method findApplyMethod() {
            try {
                Class clz = SharedPreferences.Editor.class;
                return clz.getMethod("apply");
            } catch (NoSuchMethodException e) {
            }

            return null;
        }

        /**
         * 如果找到则使用apply执行，否则使用commit
         *
         * @param editor
         */
        public static void apply(SharedPreferences.Editor editor) {
            try {
                if (sApplyMethod != null) {
                    sApplyMethod.invoke(editor);
                    return;
                }
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            editor.commit();
        }
    }

}
