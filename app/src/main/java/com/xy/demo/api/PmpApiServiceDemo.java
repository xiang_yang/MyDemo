package com.xy.demo.api;

import com.xy.demo.network.HttpResponse;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by xy on 2018/9/25.
 */

public interface PmpApiServiceDemo {
    //exam
    @GET("weather")
    Flowable<HttpResponse<Object>> getWeatherInfo(@Query("cityId") int cityId);

    //login 获取手机登陆短信验证码
    @FormUrlEncoded
    @POST("mobile/get_login_code")
    Flowable<HttpResponse<Object>> getLoginCode(@Field("phone") String phone);

    //上传头像
    @Multipart
    @POST("trainee/files")
    Flowable<HttpResponse<Object>> uploadUserImg(@Part MultipartBody.Part file);

    //updUserInfo 修改个人信息
    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded; charset=utf-8")
    @POST("trainee/edit")
    Flowable<HttpResponse<Object>> updUserInfo(@Field("id") String uuid, @Field("train_name") String name, @Field("address") String address, @Field("sex") String sex,
                                               @Field("phone") String phone, @Field("id_photo") String id_photo);

    //saveTextResult 提交测试题
    @POST("result/saveTextResult")
    Flowable<HttpResponse<Object>> submitTestResult(@Body RequestBody jsonResult);

    //获取考试规则
    @GET("paper/getAppPaper/{id}")
    Flowable<HttpResponse<Object>> getExamRule(@Path("id") String id);

    //保存考试头像
    @POST("protrait")
    Flowable<HttpResponse<Object>> uploadExamUserImg(@Body RequestBody jsonResult);

    //身份证识别
    @FormUrlEncoded
    @Multipart
    @POST("trainee/uploadIdCard")
    Flowable<HttpResponse<Object>> uploadIdCard(@Part MultipartBody.Part file, @Field("side") String side);

}
