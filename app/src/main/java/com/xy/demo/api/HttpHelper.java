package com.xy.demo.api;

import com.xy.demo.base.BaseApplication;
import com.xy.demo.bean.LoginDataBean;
import com.xy.demo.bean.VersionBean;
import com.xy.demo.network.HttpResponse;

import java.io.File;
import java.util.List;

import io.reactivex.Flowable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Query;

/**
 * Created by xy on 2018/10/19.
 */
public class HttpHelper {
    PmpApiService pmpApiService;

    public HttpHelper() {
        this.pmpApiService = BaseApplication.getRetrofit().create(PmpApiService.class);
    }

    public Flowable<HttpResponse<LoginDataBean>> login(String phone, String pwd) {
        return pmpApiService.login(phone, pwd);
    }

    public Flowable<HttpResponse<VersionBean>> getVersion(String appType, String clientVersionCode) {
        return pmpApiService.getVersion(appType, clientVersionCode);
    }

}
