package com.xy.demo.api;

import com.xy.demo.bean.LoginDataBean;
import com.xy.demo.bean.VersionBean;
import com.xy.demo.network.HttpResponse;

import java.util.List;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by xy on 2018/9/25.
 */

public interface PmpApiService {

    //login
    @FormUrlEncoded
    @POST("api/user/login")
    Flowable<HttpResponse<LoginDataBean>> login(@Field("loginName") String phone, @Field("password") String pwd);

    //getVersion
    @GET("version/checkUpdate/")
    Flowable<HttpResponse<VersionBean>> getVersion(@Query("appType") String appType, @Query("clientVersionCode") String clientVersionCode);
}
