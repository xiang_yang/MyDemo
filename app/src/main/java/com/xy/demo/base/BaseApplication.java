package com.xy.demo.base;

import android.app.Application;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.xy.demo.config.CommonConfig;
import com.xy.demo.network.interceptor.HttpHeaderInterceptor;
import com.xy.demo.network.interceptor.logging.Level;
import com.xy.demo.network.interceptor.logging.LoggingInterceptor;
import com.xy.demo.util.APKVersionUtils;
import com.xy.demo.util.LanguageUtil;
import com.xy.demo.util.LogUtils;
import com.xy.demo.util.SharePreferenceHelper;
import com.xy.demo.util.Utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApplication extends Application {

    private static BaseApplication instance;

    public static synchronized BaseApplication getInstance() {
        return instance;
    }

    private static Retrofit retrofit;
    private static OkHttpClient okHttpClient;
    public static ClearableCookieJar cookieJar;
    private static SharedPrefsCookiePersistor persistor;
    private SharePreferenceHelper mySp;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //初始化application实例
        Utils.init(this);
        mySp = Utils.getSharePreferenceHelper();
        CommonConfig.SP_TOKEN_NAME = mySp.get("token", "").toString();
        CommonConfig.SP_PHONE = mySp.get("phone", "").toString();
        CommonConfig.SP_UUID = (int) mySp.get("uuid", 0);
        CommonConfig.IS_LOGIN = (boolean) mySp.get("isLogin", false);
        LogUtils.e("Initial", "token:" + CommonConfig.SP_TOKEN_NAME);
        LogUtils.e("Initial", "phone:" + CommonConfig.SP_PHONE);
        LogUtils.e("Initial", "uuid:" + CommonConfig.SP_UUID);
        LogUtils.e("Initial", "isLogin:" + CommonConfig.IS_LOGIN);
        LogUtils.e("Initial", "版本号:" + APKVersionUtils.getVerName(getInstance()));
        initX5();
        languageInit();

    }

    private void initX5() {
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setCrashHandleCallback(new CrashReport.CrashHandleCallback() {
            public Map<String, String> onCrashHandleStart(
                    int crashType,
                    String errorType,
                    String errorMessage,
                    String errorStack) {
                LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
                String x5CrashInfo = com.tencent.smtt.sdk.WebView.getCrashExtraMessage(getApplicationContext());
                map.put("x5crashInfo", x5CrashInfo);
                return map;
            }
            @Override
            public byte[] onCrashHandleStart2GetExtraDatas(
                    int crashType,
                    String errorType,
                    String errorMessage,
                    String errorStack) {
                try {
                    return "Extra data.".getBytes("UTF-8");
                } catch (Exception e) {
                    return null;
                }
            }
        });
        CrashReport.initCrashReport(this, "com.xy.demo", true, strategy);
        HashMap map = new HashMap();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
    }

    private void languageInit() {
        String language = (String) Utils.getSharePreferenceHelper().get("LANGUAGE", "en");
        Locale appLocale = LanguageUtil.getAppLocale();
        Locale cacheLocale;
        if (language.equals("zh")) {
            cacheLocale = new Locale("zh", "CN");
        } else if (language.equals("en")) {
            cacheLocale = new Locale("en", "US");
        } else {
            cacheLocale = new Locale("en", "US");
        }
        if (!appLocale.getCountry().equals(cacheLocale.getCountry()) || !appLocale.getLanguage().equals(cacheLocale.getLanguage())) {
            LanguageUtil.changeAppLanguage(cacheLocale, false);
        }

    }

    public static Retrofit getRetrofit() {
        persistor = new SharedPrefsCookiePersistor(getInstance());
        cookieJar = new PersistentCookieJar(new SetCookieCache(), persistor);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(CommonConfig.getHost())
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
        }
        return retrofit;
    }

    private static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            //设置okhttp
            okHttpClient = new OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .cookieJar(cookieJar)
                    .addInterceptor(new HttpHeaderInterceptor())
//                    .addInterceptor(new TokenInterceptor())
//                    .addInterceptor(new DiffEquipmentLogin())
                    .addInterceptor(new LoggingInterceptor
                            .Builder()//构建者模式
                            .loggable(CommonConfig.isDebug()) //是否开启日志打印
                            .setLevel(Level.BASIC) //打印的等级
                            .log(Platform.INFO) // 打印类型
                            .build())
                    .build();
        }
        return okHttpClient;
    }

}
