package com.xy.demo.base;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xy.demo.util.gsonadapter.DoubleDefaultAdapter;
import com.xy.demo.util.gsonadapter.IntegerDefaultAdapter;
import com.xy.demo.util.gsonadapter.LongDefaultAdapter;
import com.xy.demo.util.gsonadapter.StringNullAdapter;


public abstract class BaseFragment extends Fragment {

    public View baseView;
    //ButtetKnife注解帮顶起
    public Gson gson;
    private SVProgressHUD svProgressHUD;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        baseView = inflater.inflate(getLayout(), container, false);
        gson = new GsonBuilder()
                .registerTypeAdapter(Integer.class, new IntegerDefaultAdapter())
                .registerTypeAdapter(int.class, new IntegerDefaultAdapter())
                .registerTypeAdapter(Double.class, new DoubleDefaultAdapter())
                .registerTypeAdapter(double.class, new DoubleDefaultAdapter())
                .registerTypeAdapter(Long.class, new LongDefaultAdapter())
                .registerTypeAdapter(long.class, new LongDefaultAdapter())
                .registerTypeAdapter(String.class, new StringNullAdapter())
                .create();
        initView();
        initData();
        svProgressHUD = new SVProgressHUD(getContext());
        return baseView;
    }

    protected abstract int getLayout();

    public abstract void initView();

    public abstract void initData();


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showLoading() {
        if (svProgressHUD != null && !svProgressHUD.isShowing()) {
            svProgressHUD.show();
        }
    }

    public void hideLoading() {
        if (svProgressHUD != null && svProgressHUD.isShowing()) {
            svProgressHUD.dismiss();
        }
    }
}
