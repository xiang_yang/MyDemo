package com.xy.demo.base;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xy.demo.R;
import com.xy.demo.api.HttpHelper;
import com.xy.demo.broadcast.NetworkBroadcastReceiver;
import com.xy.demo.util.LogUtils;
import com.xy.demo.util.StatusBarUtil;
import com.xy.demo.util.gsonadapter.DoubleDefaultAdapter;
import com.xy.demo.util.gsonadapter.IntegerDefaultAdapter;
import com.xy.demo.util.gsonadapter.LongDefaultAdapter;
import com.xy.demo.util.gsonadapter.StringNullAdapter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * //finish的唯一有效动画
 * ActivityOptionsCompat compat = ActivityOptionsCompat.
 * makeCustomAnimation(ContactsActivity.this, R.anim.start_enter_alpha, R.anim.start_exit_alpha);
 */
public abstract class BaseActivity extends AppCompatActivity implements NetworkBroadcastReceiver.NetworkChanged {
    //网络监测广播
    private NetworkBroadcastReceiver broadcastReceiver;
    //ButtetKnife注解帮顶起
    public Context context;

    protected HttpHelper httpHelper;
    protected String TAG = "DEBUG_TEST";
    protected final int REQUEST_CODE=10001;
    public Gson gson;

    protected CompositeDisposable mCompositeDisposable;
    private SVProgressHUD svProgressHUD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStatusBar();
        LogUtils.e("RID"+getLayout());
        setContentView(getLayout());
        context = this;
        httpHelper = new HttpHelper();
        gson = new GsonBuilder()
                .registerTypeAdapter(Integer.class, new IntegerDefaultAdapter())
                .registerTypeAdapter(int.class, new IntegerDefaultAdapter())
                .registerTypeAdapter(Double.class, new DoubleDefaultAdapter())
                .registerTypeAdapter(double.class, new DoubleDefaultAdapter())
                .registerTypeAdapter(Long.class, new LongDefaultAdapter())
                .registerTypeAdapter(long.class, new LongDefaultAdapter())
                .registerTypeAdapter(String.class, new StringNullAdapter())
                .create();
        svProgressHUD = new SVProgressHUD(context);
        initView();
        initData();
    }

    public abstract int getLayout();

    public abstract void initView();

    public abstract void initData();


    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new NetworkBroadcastReceiver(this);
        }
        //注册网络状态改变广播监听
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (broadcastReceiver != null) {
            //注销网络状态改变广播监听
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) {
            unSubscribe();
        }
    }

    @Override
    public void onNetworkChanged(boolean isNetworkConnected) {
    }


    protected void initStatusBar() {
        StatusBarUtil.setStatusBarFontAndBack(this, getResources().getColor(R.color.main));
    }

    protected void unSubscribe() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    protected void addSubscribe(Disposable subscription) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(subscription);
    }

    public void showLoading() {
        if (svProgressHUD != null && !svProgressHUD.isShowing()) {
            svProgressHUD.show();
        }
    }

    public void hideLoading() {
        if (svProgressHUD != null && svProgressHUD.isShowing()) {
            svProgressHUD.dismiss();
        }
    }
}
