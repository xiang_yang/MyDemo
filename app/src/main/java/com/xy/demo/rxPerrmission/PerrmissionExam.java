package com.xy.demo.rxPerrmission;

import android.Manifest;
import android.app.Activity;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PerrmissionExam {

    public void test(Activity activity) {

        String[] requestPermissions = {Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_MMS};
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.request(requestPermissions).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    //权限已拥有
                } else {
                    //权限被禁用
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
