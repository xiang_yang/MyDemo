package com.xy.demo.rxPerrmission;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import com.xy.demo.R;
import com.xy.demo.util.ToastUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RxPermissionUtil {
    boolean result;

    public boolean requestCameraPerm(final Activity activity) {
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    result = true;
                } else {
                    ToastUtils.showShort(activity.getApplication().getString(R.string.common_update_dialog));
                    result = false;
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
        return result;
    }
}
