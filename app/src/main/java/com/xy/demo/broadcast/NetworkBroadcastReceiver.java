package com.xy.demo.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.xy.demo.util.CheckIntel;
import com.xy.demo.util.LogUtils;

/**
 * 广播监听网络状态,
 * Created by xy on 2018/1/2.
 */

public class NetworkBroadcastReceiver extends BroadcastReceiver {

    private NetworkChanged networkChanged;

    public NetworkBroadcastReceiver(NetworkChanged networkChanged) {
        this.networkChanged = networkChanged;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtils.i("NetworkBroadcast", "网络连接状态改变");
        if (CheckIntel.isNetworkConnected(context)) {
            //有网络连接
            LogUtils.i("NetworkBroadcast", "有网络连接");
            networkChanged.onNetworkChanged(true);
        } else {
            //无网络链接
            LogUtils.i("NetworkBroadcast", "无网络连接");
            Toast.makeText(context, "您当前处于无网络状态", Toast.LENGTH_SHORT).show();
            networkChanged.onNetworkChanged(false);
        }
    }

    public interface NetworkChanged {
        void onNetworkChanged(boolean isNetworkConnected);
    }

}
