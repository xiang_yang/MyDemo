package com.xy.demo.mvp.web;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.tencent.smtt.export.external.extension.proxy.ProxyWebChromeClientExtension;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.GeolocationPermissions;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebIconDatabase;
import com.tencent.smtt.sdk.WebStorage;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.tencent.smtt.sdk.WebViewDatabase;
import com.xy.demo.R;
import com.xy.demo.base.BaseActivity;

public class WebViewActivity extends BaseActivity {

    WebView webView;

    @Override
    public int getLayout() {
        return R.layout.activity_web_view;
    }

    @Override
    public void initView() {
        webView = findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView webview, String url, String message, JsResult result) {
                //可以弹框或进行其它处理，但一定要回调result.confirm或者cancel
                //这里要返回true否则内核会进行提示
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView webview, String url, String message, JsResult result) {
                //可以弹框或进行其它处理，但一定要回调result.confirm或者cancel
                return true;
            }

            @Override
            public boolean onJsBeforeUnload(WebView webview, String url, String message, JsResult result) {
                //可以弹框或进行其它处理，但一定要回调result.confirm或者cancel
                return true;
            }

            @Override
            public boolean onJsPrompt(WebView webview, String url, String message, String defaultvalue, JsPromptResult result) {
                //可以弹框或进行其它处理，但一定要回调result.confirm或者cancel，confirm可以将用户输入作为参数
                return true;
            }
        });
        initWebViewScrolls();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //这里可以对特殊scheme进行拦截处理
                return true;//要返回true否则内核会继续处理
            }
        });
        clearCache();
        //js注入和执行
        //注入对象：
//        webView.addJavascriptInterface(Object obj, String interfaceName)
        //删除注入对象
//        webView.removeJavascriptInterface(String interfaceName)
        //执行
//        webView.evaluateJavascript(String script, ValueCallback<String> resultCallback)

        //后退 需要先判断mWebView.canGoBack()
//        webView.goBack();
        //前进 需要先判断mWebView.canGoForward()
//        webView.goForward();
        webView.setWebChromeClientExtension(new ProxyWebChromeClientExtension() {
            /**
             *页面前进后退切换完成事件通知，目前step无实际赋值，此接口只是一个完成通知
             */
            @Override
            public void onBackforwardFinished(int step) {
            }
        });
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // enable:true(日间模式)，enable：false（夜间模式）
//        webView.getSettingsExtension().setDayOrNight(true);
        //在网页未设置背景色的情况下设置网页默认背景色
//        webView.setBackgroundColor(int color);
        // 对于刘海屏机器如果webview被遮挡会自动padding
//        webView.getSettingsExtension().setDisplayCutoutEnable(true);
    }

    private void clearCache(){
//        //清除cookie
//        CookieManager.getInstance().removeAllCookies(null);
//        //清除storage相关缓存
//        WebStorage.getInstance().deleteAllData();;
//        //清除用户密码信息
//        WebViewDatabase.getInstance(Context context).clearUsernamePassword();
//        //清除httpauth信息
//        WebViewDatabase.getInstance(Context context).clearHttpAuthUsernamePassword();
//        //清除表单数据
//        WebViewDatabase.getInstance(Context context).clearFormData();
//        //清除页面icon图标信息
//        WebIconDatabase.getInstance().removeAllIcons();
//        //删除地理位置授权，也可以删除某个域名的授权（参考接口类）
//        GeolocationPermissions.getInstance().clearAll();//清除cookie
//        一次性删除所有缓存
//        QbSdk.clearAllWebViewCache(Context context,boolean isClearCookie)
    }
    private void initWebViewScrolls() {
        /**
         * Scrolls the contents of this WebView down by half the page size.
         *
         * @param bottom true to jump to bottom of page
         * @return true if the page was scrolled
         **/
//        webView.pageDown(true);

/**
 *
 * Scrolls the contents of this WebView up by half the view size.
 *
 * @param top true to jump to the top of the page
 * @return true if the page was scrolled
 **/
//        webView.pageUp( boolean top)
//滚动：
//        webView.getView().scrollTo(x, y)
/**
 * 设置滚动条样式
 */
//竖直快速滑块，设置null可去除
//        mWebview.getX5WebViewExtension().setVerticalTrackDrawable(Drawable drawable)
//判断水平滚动条是否启动
//        mWebview.getX5WebViewExtension().isHorizontalScrollBarEnabled()
//启用或禁用水平滚动条
//        mWebview.getX5WebViewExtension().setHorizontalScrollBarEnabled( boolean enabled)
//判断竖直滚动条是否启动
//        mWebview.getX5WebViewExtension().isVerticalScrollBarEnabled()
//启用或禁用竖直滚动条
//        mWebview.getX5WebViewExtension().setVerticalScrollBarEnabled( boolean enabled)
//设置滚动条渐隐消失的时间段
//        mWebview.getX5WebViewExtension().setScrollBarFadeDuration( int duration)
//设置滚动条多久开始渐隐消失
//        mWebview.getX5WebViewExtension().setScrollBarDefaultDelayBeforeFade( int delay)
    }

    @Override
    public void initData() {
        webView.loadUrl("http://www.baidu.com");
    }
}