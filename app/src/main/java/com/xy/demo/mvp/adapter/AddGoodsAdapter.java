package com.xy.demo.mvp.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.xy.demo.R;

import java.util.List;


public class AddGoodsAdapter extends RecyclerView.Adapter<AddGoodsAdapter.MyViewHolder> {

    private List<String> list;//存放数据
    private Context context;

    public AddGoodsAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public AddGoodsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.aaaa, null, false));
        return holder;
    }

    /**
     * 在这里可以获得每个子项里面的控件的实例，比如这里的TextView,子项本身的实例是itemView，
     * 在这里对获取对象进行操作
     * holder.itemView是子项视图的实例，holder.textView是子项内控件的实例
     * position是点击位置
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final AddGoodsAdapter.MyViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //这里定义的是子项的类，不要在这里直接对获取对象进行操作
    public class MyViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.text)
//        private TextView text;

        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }




    /*之下的方法都是为了方便操作，并不是必须的*/


    //去除指定位置的子项
    public boolean removeItem(int position) {
        if (position < list.size() && position >= 0) {
            list.remove(position);
            notifyItemRemoved(position);
            return true;
        }
        return false;
    }

    //清空显示数据
    public void clearAll() {
        list.clear();
        notifyDataSetChanged();
    }
}
