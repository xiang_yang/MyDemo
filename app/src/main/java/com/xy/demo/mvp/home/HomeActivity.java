package com.xy.demo.mvp.home;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.xy.demo.R;
import com.xy.demo.base.BaseActivity;
import com.xy.demo.mvp.home.fragment.BlackListFragment;
import com.xy.demo.mvp.home.fragment.HomeFragment;
import com.xy.demo.mvp.home.fragment.UserFragment;
import com.xy.demo.mvp.home.fragment.DataFragment;
import com.xy.demo.util.DensityUtil;
import com.xy.demo.util.ToolLog;
import com.xy.demo.util.dialogFragment.BaseDialog;
import com.xy.demo.util.dialogFragment.CommonDialog;
import com.xy.demo.util.dialogFragment.ViewConvertListener;
import com.xy.demo.util.dialogFragment.ViewHolder;
import com.xy.demo.util.rxPermission.PermissionUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends BaseActivity implements BottomNavigationBar.OnTabSelectedListener {

    private List<Fragment> fragmentList;
    BottomNavigationBar bnb_home_bottom;


    @Override
    public int getLayout() {
        return R.layout.activity_home;
    }


    @Override
    public void initView() {
//        initTitle("首页", 0, 0);
        // init fragment
        fragmentList = new ArrayList<>(4);
        fragmentList.add(new HomeFragment());
        fragmentList.add(new DataFragment());
        fragmentList.add(new BlackListFragment());
        fragmentList.add(new UserFragment());

        bnb_home_bottom=findViewById(R.id.bnb_home_bottom);
        bnb_home_bottom.setMode(BottomNavigationBar.MODE_FIXED);
        bnb_home_bottom.setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_DEFAULT);
        /**
         * 图标右上角红点显示提醒数量
         *               badge=new BadgeItem()
         * //                .setBorderWidth(2)//Badge的Border(边界)宽度
         * //                .setBorderColor("#FF0000")//Badge的Border颜色
         * //                .setBackgroundColor("#9ACD32")//Badge背景颜色
         * //                .setGravity(Gravity.RIGHT| Gravity.TOP)//位置，默认右上角
         *               .setText("2")//显示的文本
         * //                .setTextColor("#F0F8FF")//文本颜色
         * //                .setAnimationDuration(2000)
         * //                .setHideOnSelect(true)//当选中状态时消失，非选中状态显示
         *
         *
         .addItem(new BottomNavigationItem(R.mipmap.home_none, "悬赏封印").setActiveColor(R.color.main / R.color.white).setInActiveColor(R.color.main / R.color.white).setBadgeItem(badge))
         */
        bnb_home_bottom
                .addItem(new BottomNavigationItem(R.mipmap.home_none, "首页").setActiveColor(R.color.main / R.color.white).setInActiveColor(R.color.main / R.color.white))
                .addItem(new BottomNavigationItem(R.mipmap.document_none, "资料").setActiveColor(R.color.main / R.color.white).setInActiveColor(R.color.main / R.color.white))
                .addItem(new BottomNavigationItem(R.mipmap.black_list_none, "黑名单").setActiveColor(R.color.main / R.color.white).setInActiveColor(R.color.main / R.color.white))
                .addItem(new BottomNavigationItem(R.mipmap.user_none, "个人中心").setActiveColor(R.color.main / R.color.white).setInActiveColor(R.color.main / R.color.white))
                .setFirstSelectedPosition(0)
                .initialise();
//        setBottomNavigationItem(7, 21);
        bnb_home_bottom.setTabSelectedListener(this);
        selectFragment(0);
    }


    @Override
    public void initData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
//        checkPermissions();
    }
    /**
     * 权限正常的流程
     */
    private void onPermissionsOk() {
        ToolLog.e("onPermissionsOk", "onPermissionsOk");
    }
    /**
     * 检查应用权限
     */
    protected void checkPermissions() {
        //检查相关权限,已有权限，执行引导页跳转流程
        if (!PermissionUtil.checkPermission(this, PermissionUtil.getRequiredPermissions())) {
//            ViewUtils.hideNavigationBar(this);
            PermissionUtil.requestPermissions(this, REQUEST_CODE, PermissionUtil.getRequiredPermissions());
        } else {
            onPermissionsOk();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults != null && grantResults.length > 0) {
                    if (!PermissionUtil.checkPermission(this, PermissionUtil.getRequiredPermissions())) {
                        toSelfSetting();
                    } else {
                        onPermissionsOk();
                    }
                } else {
//                    showToast(R.string.permission_storage_fail);
                    toSelfSetting();
                }
                break;
            default:
                toSelfSetting();
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void toSelfSetting() {
        CommonDialog.newInstance()
                .setLayoutId(R.layout.diaglog_fragment_update)
                .setOutCancel(false) //点击其他地方消失
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseDialog dialog) {
                        int[] ids = new int[]{R.id.btn_dialog_ok, R.id.btn_dialog_cancel};
                        holder.setOnClickListener(ids, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                switch (view.getId()) {
                                    case R.id.btn_dialog_ok:
                                        Intent mIntent = new Intent();
                                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                                        mIntent.setData(Uri.fromParts("package", getPackageName(), null));
                                        startActivity(mIntent);
                                        dialog.dismiss();
                                        break;
                                    case R.id.btn_dialog_cancel:
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });
                    }
                }).show(getSupportFragmentManager());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
            tran.add(R.id.fl_home_fragment, fragmentList.get(0));
            tran.commitAllowingStateLoss();
        }
    }

    //未选中 -> 选中
    @Override
    public void onTabSelected(int position) {
        //开启事务
        //开启事务
        selectFragment(position);
    }

    private Fragment currentFragment;
    private void selectFragment(int position) {
        Fragment fragment = fragmentList.get(position);
        if (fragment != null && fragment != currentFragment) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragment.isAdded()) {
                if (currentFragment != null && currentFragment != fragment) {
                    transaction.hide(currentFragment).show(fragment);
                } else {
                    transaction.show(fragment);
                }
            } else {
                if (currentFragment != null) {
                    transaction.hide(currentFragment).add(R.id.fl_home_fragment, fragment);
                } else {
                    transaction.add(R.id.fl_home_fragment, fragment);
                }
            }
            currentFragment = fragment;
            transaction.commitAllowingStateLoss();
            bnb_home_bottom.selectTab(position);
        }
    }


    //选中 -> 未选中
    @Override
    public void onTabUnselected(int position) {

    }

    //选中 -> 选中
    @Override
    public void onTabReselected(int position) {

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) {
            bnb_home_bottom.setTabSelectedListener(null);
        }
    }

    //使用反射改变BottomNavigationItem的文图间距(同时改变字体大小) 和 图片大小
//    private void setBottomNavigationItem(int space, int imgLen) {
//        float contentLen = 36;
//        Class barClass = bnb_home_bottom.getClass();
//        Field[] fields = barClass.getDeclaredFields();
//        for (int i = 0; i < fields.length; i++) {
//            Field field = fields[i];
//            field.setAccessible(true);
//            if (field.getName().equals("mTabContainer")) {
//                try { //反射得到 mTabContainer
//                    LinearLayout mTabContainer = (LinearLayout) field.get(bnb_home_bottom);
//                    for (int j = 0; j < mTabContainer.getChildCount(); j++) {
//                        //获取到容器内的各个 Tab
//                        View view = mTabContainer.getChildAt(j);
//                        //获取到Tab内的各个显示控件
//                        // 获取到Tab内的文字控件
//                        TextView labelView = (TextView) view.findViewById(com.ashokvarma.bottomnavigation.R.id.fixed_bottom_navigation_title);
//                        //计算文字的高度DP值并设置，setTextSize为设置文字正方形的对角线长度，所以：文字高度（总内容高度减去间距和图片高度）*根号2即为对角线长度，此处用DP值，设置该值即可。
//                        labelView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float) (Math.sqrt(2) * (contentLen - imgLen - space)));
//                        //获取到Tab内的图像控件
//                        ImageView iconView = (ImageView) view.findViewById(com.ashokvarma.bottomnavigation.R.id.fixed_bottom_navigation_icon);
//                        //设置图片参数，其中，MethodUtils.dip2px()：换算dp值
//                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((int) DensityUtil.dip2px(imgLen), (int) DensityUtil.dip2px(imgLen));
//                        params.gravity = Gravity.CENTER;
//                        iconView.setLayoutParams(params);
//                    }
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}
