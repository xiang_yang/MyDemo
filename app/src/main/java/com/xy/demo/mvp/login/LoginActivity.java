package com.xy.demo.mvp.login;

import android.Manifest;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xy.demo.R;
import com.xy.demo.base.BaseActivity;
import com.xy.demo.bean.LoginDataBean;
import com.xy.demo.config.CommonConfig;
import com.xy.demo.mvp.forget.ForgetPwdActivity;
import com.xy.demo.mvp.home.HomeActivity;
import com.xy.demo.network.CommonHandleResult;
import com.xy.demo.rxPerrmission.RxPermissions;
import com.xy.demo.rxUtil.RxUtil;
import com.xy.demo.util.DownloadApk;
import com.xy.demo.util.LogUtils;
import com.xy.demo.util.RxTimerUtil;
import com.xy.demo.util.SharePreferenceHelper;
import com.xy.demo.util.ToastUtils;
import com.xy.demo.util.ToolLog;
import com.xy.demo.util.Utils;


import java.util.Date;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class LoginActivity extends BaseActivity {

    EditText et_login_loginName;
    EditText et_login_pwd;
    Button bt_login;
    CheckBox cb_savePwd;
    TextView tv_login_forgetPwd;
    LinearLayout ll_login;
    TextView iv_login_icon_text;
    private String version;
    //数据持久化
    SharePreferenceHelper mySP;
    private Animator animator;
    private String username;
    private String password;


    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
//        if (CommonConfig.isLogin()){
//            Utils.finishThis(this,this);
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//        }

//        initTitle(getResources().getString(R.string.login), 0, 0);
        version = getVersionName();

    }


    @Override
    public void initData() {
        mySP = Utils.getSharePreferenceHelper();
//        requestCameraPerm();
        requestAllPerm();
    }

    private RxTimerUtil rxTimerUtil;
    @Override
    protected void onResume() {
        super.onResume();
        if (rxTimerUtil == null) {
            rxTimerUtil = new RxTimerUtil();
        }
        rxTimerUtil.interval(5000, new RxTimerUtil.IRxNext() {
            @Override
            public void doNext(long number) {
                long time = new Date().getTime() / 1000 - 1598797800;
                ToolLog.e("定时器启动", "定时器启动" + time);
            }
        });
    }

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_login:
                username = et_login_loginName.getText().toString();
                password = et_login_pwd.getText().toString();
                //请求数据 成功后跳转
//                if (username.length() > 0) {
//                    if (password.length() > 0) {
//                        if (password.length() > 5) {
//                            //继续登陆
//                            bt_login.startAnim();
//                            addSubscribe(httpHelper.login(username, password)
//                                    .compose(RxUtil.<HttpResponse<LoginDataBean>>rxSchedulerHelper())
//                                    .compose(CommonHandleResult.<LoginDataBean>handleReuslt())
//                                    .subscribeWith(new CommonSubscriber<LoginDataBean>() {
//                                        @Override
//                                        public void onNext(LoginDataBean dataBean) {
//                                            ToolLog.e("接口访问", "登陆接口请求成功");
//                                            LoginDataBean.UserBean user = dataBean.getUser();
//                                            CommonConfig.SP_USERNAME = et_login_loginName.getText().toString();
//                                            CommonConfig.SP_PASSWORD = et_login_pwd.getText().toString();
//                                            CommonConfig.SP_PHONE = user.getUphone();
//                                            CommonConfig.SP_UUID = user.getUserID();
//                                            CommonConfig.SP_TOKEN_NAME = dataBean.getToken();
//
//                                            mySP.put("loginName", CommonConfig.SP_USERNAME);
//                                            mySP.put("loginPwd", CommonConfig.SP_PASSWORD);
//                                            mySP.put("uuid", user.getUserID());
//                                            mySP.put("phone", user.getUphone());
//                                            mySP.put("token", CommonConfig.SP_TOKEN_NAME);
//
//                                            if (cb_savePwd.isChecked()) {
//                                                CommonConfig.IS_LOGIN = true;
//                                            }
//                                            //跳转首页
//                                        }
//
//                                        @Override
//                                        public void onError() {
//                                            if (bt_login != null) {
//                                                bt_login.regainBackground();
//                                                bt_login.setClickable(true);
//                                            }
//                                            ToolLog.e("接口访问", "登陆接口请求失败");
//                                            ToastUtils.showShort("登陆失败，请重新登陆。");
//                                        }
//                                    }));
//                        } else {
//                            ToastUtils.showLong("密码最少为6位，请重新输入密码");
//                        }
//                    } else {
//                        ToastUtils.showLong("请输入密码");
//                    }
//                } else {
//                    ToastUtils.showLong("请输入账号");
//                }
                break;
            case R.id.tv_login_forgetPwd:
                startActivity(new Intent(this, ForgetPwdActivity.class));
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ToolLog.e("onPause", "onPause");
        if (rxTimerUtil != null) {
            //一定要在onPause里面取消
            rxTimerUtil.cancel();
            rxTimerUtil = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) {
        }
    }

    public String getVersionName() {
        // 获取packagemanager的实例
        PackageManager packageManager = this.getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert packInfo != null;
        return packInfo.versionName;
    }


    private void showDialog(final String url) {
        AlertDialog.Builder layoutDialog = new AlertDialog.Builder(Utils.getApp());//通过AlertDialog.Builder创建出一个AlertDialog的实例
        final AlertDialog dialog = layoutDialog.create();
        View dialogView = LayoutInflater.from(Utils.getApp()).inflate(R.layout.diaglog_fragment_update, null);
        dialogView.findViewById(R.id.btn_dialog_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_dialog_ok:
                        loadNewVersionProgress(url);
                        break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new BitmapDrawable());
        dialog.getWindow().setContentView(dialogView);//自定义布局应该在这里添加，要在dialog.show()的后面
    }



    private void loadNewVersionProgress(final String apkUrl) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setMessage("正在更新... ");
        pd.show();
        new Thread(new DownloadApk(pd, apkUrl, LoginActivity.this)).start();
    }

    /**
     * 监听back键处理DrawerLayout和SearchView
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return true;
    }

    /**
     * 请求权限
     */
    private void requestAllPerm() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    LogUtils.e("当前版本" + version);
                    //请求版本号并处理

                    //请求成功
//                    loadNewVersionProgress(文件url);
                } else {
                    ToastUtils.showShort(getString(R.string.common_update_dialog));
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }
}
