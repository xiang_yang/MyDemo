package com.xy.demo.mvp.splash;

import android.content.Intent;

import com.xy.demo.R;
import com.xy.demo.base.BaseActivity;
import com.xy.demo.config.CommonConfig;
import com.xy.demo.mvp.home.HomeActivity;
import com.xy.demo.mvp.login.LoginActivity;
import com.xy.demo.util.RxTimerUtil;
import com.xy.demo.util.Utils;


public class SplashActivity extends BaseActivity {


    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void initView() {
    }



    @Override
    public void initData() {
        Intent intent;
        if (CommonConfig.isLogin()) {
            intent = new Intent(SplashActivity.this, HomeActivity.class);
        } else {
            intent = new Intent(SplashActivity.this, LoginActivity.class);
        }
        new RxTimerUtil().delayNext(1500, new RxTimerUtil.IRxNext() {
            @Override
            public void doNext(long number) {
                startActivity(intent);
                Utils.finishThis(context, SplashActivity.this);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}