package com.xy.demo.mvp.home.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.xy.demo.R;
import com.xy.demo.base.BaseFragment;
import com.xy.demo.mvp.web.WebViewActivity;


public class HomeFragment extends BaseFragment {

    @Override
    protected int getLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void initView() {
        Button btn_dataShow=baseView.findViewById(R.id.btn_dataShow);
        btn_dataShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), WebViewActivity.class));
            }
        });
    }

    @Override
    public void initData() {

    }
}
