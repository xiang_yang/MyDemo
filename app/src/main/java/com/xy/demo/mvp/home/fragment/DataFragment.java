package com.xy.demo.mvp.home.fragment;

import com.xy.demo.R;
import com.xy.demo.base.BaseFragment;


public class DataFragment extends BaseFragment {

    @Override
    protected int getLayout() {
        return R.layout.fragment_data;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }
}
