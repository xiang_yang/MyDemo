package com.xy.demo.mvp.forget;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.xy.demo.R;
import com.xy.demo.util.Utils;


public class ForgetPwdActivity extends AppCompatActivity {

    ImageView iv_title_left;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
        iv_title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish的唯一有效动画
                Utils.finishThis(ForgetPwdActivity.this, ForgetPwdActivity.this);
            }
        });
    }

}
